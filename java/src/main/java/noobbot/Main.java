package noobbot;

import java.io.*;
import java.net.Socket;

import com.google.gson.Gson;
import hardest.entity.CarId;
import hardest.entity.CarPosition;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
        System.out.println("Connection is over");
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        CarId myCar = null;
        double currentThrottle = 0.75;

        //For debuging angle
        //FileWriter fileWriter = new FileWriter(new File("data.txt"));

        int count = 10;
        double lastAngle = 180;
        float ratio = 1.0f;

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                //A hack to get Object to JSON to Desired Object
                //Can be replaced by parsing the same line twice
                CarPosition[] carPositions = gson.fromJson(gson.toJson(msgFromServer.data), CarPosition[].class);

                for (CarPosition carPosition : carPositions) {
                    if (myCar != null && carPosition.id.name.equals(myCar.name)) {

                        double absAngle = Math.abs(carPosition.angle);
                        if (lastAngle < absAngle) {
                            currentThrottle -= 0.1*ratio;
                            ratio += 1.0f;
                        } else {
                            currentThrottle += 0.1f;
                            ratio = 1.0f;
                        }
                        lastAngle = absAngle;


                        currentThrottle = Math.min(1, Math.max(0.01, currentThrottle));
                        //fileWriter.write(carPosition.angle+" : "+currentThrottle+"\n");
                        break;
                    }
                }
                /*count--;
                if (count == 0) {
                    fileWriter.flush();
                    count = 10;
                }*/
                send(new Throttle(currentThrottle));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("yourCar")) {
                myCar = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
                send(new Ping());
            } else {
                System.out.println(msgFromServer.msgType);
                send(new Ping());
            }
        }

        //fileWriter.close();
        writer.close();
        reader.close();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
package hardest.entity;

/**
 * Created by aurimas on 25/04/14.
 */
public class CarPiecePosition {

    public int pieceIndex;
    public float inPieceDistance;
    public CarLane lane;
    public int lap;
}

package hardest.entity;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {
    public float length;
    @SerializedName("switch")
    public boolean hasSwitch;
    public float radius;
    public float angle;
}

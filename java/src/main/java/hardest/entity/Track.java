package hardest.entity;

public class Track {
    public String id;
    public String name;
    public TrackPiece[] pieces;
    public TrackLane[] lanes;
    public AnglePoint startingPoint;

}
